helpBot = {}

function helpBot.whyAreYou(text, pattern)
	if text:find(pattern) then
		local answer = text:sub(text:find(pattern) + #pattern + 1)
		answer:gsub("you","ME_1")
		answer:gsub("me","you")
		answer:gsub("ME_1","me")
		print("Why are you " .. answer .. "?")
		return true
	else
		return false
	end
end

helpBot.patterns= {
	ending = {"gtg", "bye", "got to go", "exit", "quit"},
	welcome = {"hello", "hi"},
	imHelpingYou = {"you're", "you are", "that's mean", "I'm not sure I understand you"},
	whoAreYou = {"Who are you", "I'll call you", "who're you", "I will call you"},
}

function findAny(text, patterns)
	for _,v in pairs(patterns) do
		local idx = text:find(v)
		if idx then
			local nextChar = idx + #v
			if #text < idx + #v or text:sub(nextChar,nextChar) == " " then
				return true
			end
		end
	end
	return false
end

function helpBot.getMessage(text)
	local patterns = helpBot.patterns
	text = text:gsub("%.","")
	if findAny(text, patterns.whoAreYou) then
		print("I'm just a chat bot, call me whatever you like.")
	elseif findAny(text, patterns.welcome) then
		print("Hello there, can I help you?")
	elseif findAny(text, patterns.ending) then
		print("Hope to see you soon!");
		helpBot.quitChat = true
	elseif helpBot.whyAreYou(text,"I am") then
	elseif helpBot.whyAreYou(text,"I'm") then
	elseif findAny(text, patterns.imHelpingYou) then
		print("Please don't blame me for what I'm saying, I'm just trying to help you.");
	elseif text == "" then
		--nothing
	else
		print("I'm not sure I understand you.")
	end
end

helpBot.quitChat = false

function helpBot.chat()
	while (not helpBot.quitChat) do
		text = io.read()
		helpBot.getMessage(text)
	end
end

helpBot.chat()
